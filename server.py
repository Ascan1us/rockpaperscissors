import socket
import random
import redis
import threading

class RockPaperScissorsGame:
    def __init__(self):
        self.choices = ['rock', 'paper', 'scissors']

    def make_choice(self):
        return random.choice(self.choices)

    def mechanics(self, data, c_choice):
        if data == c_choice:
            return "Draw"
        elif (
            (data == 'rock' and c_choice == 'scissors') or
            (data == 'paper' and c_choice == 'rock') or
            (data == 'scissors' and c_choice == 'paper')
        ):
            return "Win!"
        else:
            return "Lose"

class Server:
    def __init__(self, host, port, game, redis_host, redis_port):
        self.host = host
        self.port = port
        self.game = game
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((host, port))
        self.server_socket.listen()
        print(f"Server listening on {self.host}:{self.port}")
        self.redis_client = redis.StrictRedis(host=redis_host, port=redis_port)

    def processing(self, conn, addr):
        print(f"Connection from {addr}")
        player_name = conn.recv(1024).decode("utf-8")
        try:
            while True:
                c_choice = self.game.make_choice()
                data = conn.recv(1024).decode("utf-8")
                if data == 'exit':
                    break
                try:
                    result = self.game.mechanics(data, c_choice)
                    game_result = f"{player_name} choose: {data}\nComputer choose: {c_choice}\nResult: {result}"
                    self.redis_client.rpush('results', game_result)
                    conn.send(bytes(game_result, encoding="utf-8"))
                except Exception as e:
                    print(f"Error: {e}")
                    conn.send(bytes("Error", encoding="utf-8"))
        finally:
            conn.close()

    def start(self):
        try:
            while True:
                conn, addr = self.server_socket.accept()
                threading.Thread(target=self.processing, args=(conn, addr)).start()
        finally:
            self.server_socket.close()

if __name__ == '__main__':
    game = RockPaperScissorsGame()
    server = Server('127.0.0.1', 55000, game, redis_host="", redis_port="")   
    server.start()
