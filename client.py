import socket

class RockPaperScissorsGame:
    def __init__(self):
        self.choices = ['rock', 'paper', 'scissors']

class ClientServer:
    def __init__(self, host, port, game):
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = host
        self.port = port
        self.game = RockPaperScissorsGame()

    def connect(self):
        self.client_socket.connect((self.host, self.port))

    def close(self):
        self.client_socket.close()

    def run(self):
        try:
            player_name = input("Enter your name: ")
            self.client_socket.send(bytes(player_name, encoding="utf-8"))
            while True:
                choice = input("Enter your choice (rock, paper, scissors) or 'exit' to end: ").lower()
                self.client_socket.send(bytes(choice, encoding="utf-8"))
                if choice == 'exit':
                    break
                if choice not in self.game.choices:
                    print("Invalid choice. Please enter rock, paper, or scissors.")
                    continue
                response = self.client_socket.recv(1024).decode("utf-8")
                print(response)
        finally:
            self.close()

if __name__ == '__main__':
    game = RockPaperScissorsGame()
    knb = ClientServer('127.0.0.1', 55000, game)
    knb.connect()
    knb.run()

